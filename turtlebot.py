import uuid
import random
import rosbag
import rospkg

from std_msgs.msg import Header
from math import *

class Bot():

	def __init__(self, map_size):
		self.name = uuid.uuid1()
		
		self.map_size = map_size

		self.x = int(random.random() * map_size["width"])
		self.y = int(random.random() * map_size["height"])
        	self.orientation = random.random() * 2.0 * pi
        	self.forward_noise = 0.0;
        	self.turn_noise    = 0.0;
       		self.sense_noise   = 0.0;
	
	def get_location(self):
		location = {}
		location["x"] = self.x
		location["y"] = self.y
	
		return location

	def set_noise(self, new_f_noise, new_t_noise, new_s_noise):
	        self.forward_noise = float(new_f_noise);
        	self.turn_noise    = float(new_t_noise);
        	self.sense_noise   = float(new_s_noise);
 
	def set(self, new_x, new_y, new_orientation):
        	if new_x < 0 or new_x >= self.map_size["width"]:
            		raise ValueError, 'X coordinate out of bound'
        	
		if new_y < 0 or new_y >= self.map_size["height"]:
            		raise ValueError, 'Y coordinate out of bound'
        	
		if new_orientation < 0 or new_orientation >= 2 * pi:
            		raise ValueError, 'Orientation must be in [0..2pi]'
        	
		self.x = int(new_x)
        	self.y = int(new_y)
        	self.orientation = float(new_orientation)

	def move(self, turn, forward):
        	# turn, and add randomness to the turning command
        	orientation = self.orientation + float(turn) + random.gauss(0.0, self.turn_noise)
        	orientation %= 2 * pi

        	# move, and add randomness to the motion command
        	dist = float(forward) + random.gauss(0.0, self.forward_noise)
        	x = self.x + (cos(orientation) * dist)
        	y = self.y + (sin(orientation) * dist)
        	x %= self.map_size["width"]    # cyclic truncate
        	y %= self.map_size["height"]

        	# set particle
        	res = Bot(self.map_size)
        	res.set(x, y, orientation)
        	res.set_noise(self.forward_noise, self.turn_noise, self.sense_noise)
        	
		return res
	
	def measurement_prob(self, measurement, screen):

	        prob = 1.0;

        	comp = self.sense(screen)


	        for i in range(len(measurement)):
            		p = 1.
            		dist = abs(measurement[i] - comp[i])
            		
			if dist == 0:
                		p = 1.
            		else:
                		p = 1. / dist

            		prob *= p

	        return prob
	
	def hardwareSense(self, rospy):
		TIME_OFFSET_ANGULAR_SPEED = 0.02
		TIME_OFFSET_POLAR_LASER_DATA = 0.4
		
		rp = rospkg.RosPack()
                path = rp.get_path('turtlebot_laser_work')
                bag = rosbag.Bag(path + '/scripts/laser_and_odom.bag', 'r')

                laser_msgs = []
                for topic, msg, t in bag.read_messages():
                        if topic == "scan":
                                laser_msgs.append(msg)
		
		laser_yielder = self.yielder(laser_msgs)

		for iteration in range(int(TIME_OFFSET_POLAR_LASER_DATA / TIME_OFFSET_ANGULAR_SPEED)): # we publish 20 times
            		rospy.sleep(TIME_OFFSET_ANGULAR_SPEED)
        	
		try:
            		curr_laser = laser_yielder.next()
        	except StopIteration: # laser data finishes before odometry data
			print("error")
		
		curr_laser.header.stamp = rospy.Time.now()		
                #laser_pub.publish(curr_laser)


	def pixelSense(self, screen, draw=False):
	        DETECTIONCOLOR = (1, 1, 1, 255)

	        DIRECTIONS = [-2.5,-1,0.,1,2.5]

	        SENSORSTEP = 3.

	        SENSORDISTANCE = 30.

	        dist = []

        	for i in range(len(DIRECTIONS)):
	            	tempX = self.x
        	    	tempY = self.y
           	 	detected = False
            		distance = 0
            		dX = (cos(self.orientation + DIRECTIONS[i]) * SENSORSTEP)
            		dY = (sin(self.orientation + DIRECTIONS[i]) * SENSORSTEP)

			while (int(tempX) > 0) and (int(tempY) > 0) and (int(tempX) < self.map_size["width"]) and (int(tempY) < self.map_size["height"]) and not detected and (distance < SENSORDISTANCE):

	                	distance = distance + 1
           			tempX = tempX + dX
                		tempY = tempY + dY

	                	try:
                    			detectedColor = screen.get_at(((int(tempX), int(tempY))))

                    			if draw:
                        			screen.set_at((int(tempX), int(tempY)), (255,0,0,255))

                    			if detectedColor == DETECTIONCOLOR:
                        			detected = True
                		except:
                    			a = 1
            	
			dist.append(distance)

        	return dist
	
	def yielder(self, list):
		counter = 0
		while counter < len(list):
        		for item in list:
            			yield item
            			counter += 1
