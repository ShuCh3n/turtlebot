from geometry_msgs.msg import Twist
import turtlebot
import Image
from math import *
from sensor_msgs.msg import LaserScan

class Filter():
	def __init__(self, rospy):
		
		self.rospy = rospy
		rospy.Publisher('/scan', LaserScan)

		self.particles = 3000
		
		pgm_file = Image.open('shu_woonkamer.pgm')
		pgm_file.convert('RGB').save("filter_maps/map.jpg", "JPEG")

		self.map = Image.open('filter_maps/map.jpg')
		map_width, map_height = self.map.size
		
		pixel_map = self.map.load()		
		
		self.map_size = {}
		self.map_size["width"] = map_width
		self.map_size["height"] = map_height
	
		self.myTB = turtlebot.Bot(self.map_size)
		self.myTB.set(180.0, 20.0, pi/2.)

		self.p = []
		for i in range(self.particles):
			tb = turtlebot.Bot(self.map_size)

			tb.set_noise(5.5, 5.5, 0.0)
			self.p.append(tb)
			
			particle_location = tb.get_location()
			
			self.map.putpixel((particle_location["x"], particle_location["y"]), (255, 0, 0))
		
		self.map.save("filter_maps/map_add_particles.jpg", "JPEG")		

	def getRospy(self):
		return self.rospy
		
	def detect(self):
		self.cmd_vel = self.rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)

		#TurtleBot will stop if we don't keep telling it to move.  How often should we tell it to move? 10 HZ
		r = self.rospy.Rate(10);
		
		# Twist is a datatype for velocity
		move_cmd = Twist()
		# let's go forward at 0.2 m/s
		move_cmd.linear.x = 0.2
		# let's turn at 0 radians/s
		move_cmd.angular.z = 0
		
		#scan around
		for x in range(0,24):
			for y in range(0, 8):
				self.spinAround()
				r.sleep()

			self.moveAndUpdate(pi/-12., 0)


		return True
		# # as long as you haven't ctrl + c keeping doing...
  #       	while not self.rospy.is_shutdown():
		# 	# publish the velocity
  #           		self.cmd_vel.publish(move_cmd)
			
		# 	self.moveAndUpdate(0, 5)
		# 	# wait for 0.1 seconds (10 HZ) and publish again
		# 	r.sleep()
	
	def spinAround(self):
		turn_cmd = Twist()
		turn_cmd.linear.x = 0
		turn_cmd.angular.z = radians(15)
		self.cmd_vel.publish(turn_cmd)

	def moveAndUpdate(self, rotation, movement):
		map = Image.open('filter_maps/map.jpg')

		self.myTB = self.myTB.move(rotation,movement)
		
		#Z = self.myTB.hardwareSense(self.rospy)
		
		w = []
		for i in range(len(self.p)):
			self.p[i] = self.p[i].move(rotation, movement)
			particle_location = self.p[i].get_location()
			
			map.putpixel((particle_location["x"], particle_location["y"]), (255, 0, 0))

			w.append(self.p[i])
	#	w.append(self.particles[i].measurement_prob(Z, self.screen))
		
		map.save("filter_maps/map_" + str(self.rospy.Time.now())  + ".jpg", "JPEG")

	def shutdown(self):
        	# stop turtlebot
        	self.rospy.loginfo("Stop TurtleBot")
        	
		# a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
        	self.cmd_vel.publish(Twist())
        
		# sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
        	self.rospy.sleep(1)
