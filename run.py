import os
import rospy
import sys
import requests
import time
import drive
import amc
from geometry_msgs.msg import Twist

if __name__ == '__main__':

	try:
		rospy.init_node('stayalive', anonymous=False)
	except rospy.ROSInterruptException:
		rospy.loginfo("Ctrl-C caught. Quitting")

	# Sleep to give the last log messages time to be sent
	rospy.sleep(1)

	while True:	
		request = requests.get('http://stayalive.shuch3n.com/api/action', headers={'AppToken':'pbMSeUfUP6Nmfoz2ScxtTPOj8N5WrDXVLBtynWIc'})
		data = request.json()

		if(data['status']):
			if(data['action']['type_id'] == 1):
				x = 0
				y = 0

				if(data['action']['plant']['x']):
					x = data['action']['plant']['x']
				
				if(data['action']['plant']['y']):
					y = data['action']['plant']['y']
				
				robot = drive.Drive(rospy)
				robot.goto(float(x), float(y))

			if(data['action']['type_id'] == 2):
				robot = amc.Filter(rospy)
				robot.detect()

			requests.get('http://stayalive.shuch3n.com/api/action/finish', headers={'AppToken':'pbMSeUfUP6Nmfoz2ScxtTPOj8N5WrDXVLBtynWIc'})
			
			# What function to call when you ctrl + c    
			rospy = robot.getRospy()
			rospy.on_shutdown(robot.shutdown)

		time.sleep(2)
        
	# if(str(sys.argv[1]) == "goto"):
	# 	robot = drive.Drive(rospy)
 #        	robot.goto(float(sys.argv[2]), float(sys.argv[3]))
		
	# if(str(sys.argv[1]) == "amc"):
	# 	robot = amc.Filter(rospy)
	# 	robot.detect()


