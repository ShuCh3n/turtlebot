from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, Point, Quaternion
from geometry_msgs.msg import Twist

class Drive():
	def __init__(self, rospy):
		self.rospy = rospy
		self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
		self.move_base.wait_for_server(rospy.Duration(5))
	
	def getRospy(self):
		return self.rospy

	def goto(self, x_point, y_point):
		self.cmd_vel = self.rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)
		r = self.rospy.Rate(10);

		move_cmd = Twist()
		move_cmd.linear.x = 0.2

		for x in range(0,15):
			self.cmd_vel.publish(move_cmd)
			r.sleep()
		# goal = MoveBaseGoal()
		# goal.target_pose.header.frame_id = 'map'
		# goal.target_pose.header.stamp = self.rospy.Time.now()
		# goal.target_pose.pose = Pose(Point(x_point, y_point, 0.000), Quaternion(0.0, 0.0, 0.0, 1.0))
		# self.move_base.send_goal(goal)

	def shutdown(self):
		# stop turtlebot
		self.rospy.loginfo("Stop TurtleBot")
		
	# a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
		self.cmd_vel.publish(Twist())

	# sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
		self.rospy.sleep(1)